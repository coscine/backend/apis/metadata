﻿using System;
using Newtonsoft.Json;

namespace Coscine.Api.Metadata.ParameterObjects
{
    /// <summary>
    ///  Parameter object containing the application profile informations.
    /// </summary>
    public class ApplicationProfile
    {
        /// <summary>
        ///  Name of the application profile
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        ///  Base URL of the application profile
        /// </summary>
        [JsonProperty("base_url")]
        public string BaseURL { get; set; }
        /// <summary>
        ///  Definition of the application profile
        /// </summary>
        [JsonProperty("definition")]
        public string Definition { get; set; }
        /// <summary>
        ///  Mime type of the application profile
        /// </summary>
        [JsonProperty("mimeType")]
        public string MimeType { get; set; }
    }
}
