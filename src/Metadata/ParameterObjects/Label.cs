﻿using Newtonsoft.Json;

namespace Coscine.Api.Metadata.ParameterObjects
{
    /// <summary>
    /// Label of a vocabulary entry
    /// </summary>
    public class Label
    {
        /// <summary>
        ///  Name of the application profile
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        ///  Name of the application profile
        /// </summary>
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
