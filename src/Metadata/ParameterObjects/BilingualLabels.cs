﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Coscine.Api.Metadata.ParameterObjects
{
    /// <summary>
    /// Bilingual labels
    /// </summary>
    public class BilingualLabels
    {
        /// <summary>
        /// English labels
        /// </summary>
        [JsonProperty("en")]
        public ICollection<Label> En { get; set; } = new List<Label>();
        /// <summary>
        /// German labels
        /// </summary>
        [JsonProperty("de")]
        public ICollection<Label> De { get; set; } = new List<Label>();
    }
}
